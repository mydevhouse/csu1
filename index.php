<?php
	include('config.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>OFFICIALS WEBSITE OF ENRILE</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="assets/css/bootstrap.css" rel="stylesheet">
<link href="assets/css/bootstrap-responsive.css" rel="stylesheet">
<link href="assets/css/docs.css" rel="stylesheet">
<link href="assets/js/google-code-prettify/prettify.css" rel="stylesheet">
<!--[if lt IE 9]>
<script src="assets/js/html5.js"></script>
<![endif]-->
<link rel="shortcut icon" href="assets/ico/favicon.ico">
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">
</head>
<body data-spy="scroll" data-target=".bs-docs-sidebar">
<div class="jumbotron masthead">
  <div class="nav-agency">
    <div class="navbar navbar-static-top">
      <div class="navbar-inner">
        <div class="container"> <a class="brand" href="."> <img src="imgs/logo2.png" alt=""></a>
          <div id="main-nav">
            <div class="nav-collapse collapse">
              <ul class="nav">
                <li class="active"><a href="index.php">HOME</a> </li>
                <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">ABOUT US <b class="caret"></b></a>
                  <ul class="dropdown-menu">
					<li><a href="history.php">History</a></li>
					<li><a href="mission-vision.php">Mission & Vision</a></li>
                      <li><a href="barangay.php">Barangay</a></li>
                  </ul>
                </li>
                <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#"> GALLERY<b class="caret"></b></a>
                  <ul class="dropdown-menu">
                    <li><a href="festival.php">Mappalabbet Festival</a></li>
                    <li><a href="tourist-spot.php">Tourist Spot</a></li>
                  </ul>
                </li>
                <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"> OFFICIALS <b class="caret"></b></a>
                  <ul class="dropdown-menu">
                    <li><a href="elected-officials.php">Elected Official</a></li>
                    <li><a href="department-heads.php">Department Heads</a></li>
                  </ul>
                </li>
                   <li><a href="news-events.php">NEWS & EVENTS </a></li>
                <li><a href="contact.php">CONTACT</a> </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
 <div class="splash"> <img src="imgs/cover.jpg" alt=""> </div>
  <div class="splash"> <img src="imgs/musang.jpg" alt=""> </div>
  <div class="splash"> <img src="imgs/1.jpg" alt=""> </div>
  <div class="container show-case-item">
    <h1>OFFICIALS WEBSITE OF ENRILE CAGAYAN</h1>
    <p> </p>
    <a href="work.html" class="bigbtn">View Our Work</a>
    <div class="clearfix"> </div>
  </div>
  <div class="container show-case-item">
    <h1>SIMPLICITY IS A GOOD THING<br>
      ADOPT!</h1>
    <p>Gone are the days of building simple websites. Clients are demanding more functionality and better results from their websites and we create unforgettable brand experiences. Our passion is helping design and build solutions that strike the perfect balance between users, business, and technology.</p>
    <a href="work.html" class="bigbtn">View Our Work</a>
    <div class="clearfix"> </div>
  </div>
  <div class="container show-case-item">
    <h1>PLAN, BUILD, LAUNCH<br>
      &amp; GROW!</h1>
    <p>Gone are the days of building simple websites. Clients are demanding more functionality and better results from their websites and we create unforgettable brand experiences. Our passion is helping design and build solutions that strike the perfect balance between users, business, and technology.</p>
    <a href="work.html" class="bigbtn">View Our Work</a>
    <div class="clearfix"> </div>
  </div>
  <div id="banner-pagination">
    <ul>
      <li><a href="#" class="active" rel="0"> <img src="assets/img/slidedot-active.png" alt=""></a></li>
      <li><a href="#" rel="1"> <img src="assets/img/slidedot.png" alt=""></a></li>
      <li><a href="#" rel="2"> <img src="assets/img/slidedot.png" alt=""></a></li>
    </ul>
  </div>
</div>
<div class="container">
  <div class="marketing">
  
    <p class="marketing-byline"></p>
    <hr class="soften">
    <div class="row-fluid">
    
        
        
 
 <body> 
 <div class="span12">
<div class="col-md-6">
<video width="400" controls>
  <source src="mov_bbb.mp4" type="video/mp4">
  <source src="mov_bbb.ogg" type="video/ogg">
  Your browser does not support HTML5 video.
</video>

<p>
Video courtesy of 
<a href="https://www.bigbuckbunny.org/" target="_blank">ENRILE VIDEO</a>.
</p>
</div>
<div class="col-md-6">
 <h3>Title here</h3>
 <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt 
 ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco 
 laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate
 velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, 
 sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
</div>
</div>
    <hr class="soften">
    <div class="row-fluid textleft">
      <div class="span12">
	  <br><br>
        <h2> <center><span class="firstword">NEWS</span> </center></h2>
        <p>       <?php
//									$sqlmax=mysqli_query($dbcon,"select MAX(ID) from tblnews");
//									$rowmax=mysqli_fetch_array($sqlmax);
//									$maxid=$rowmax[0];
								    $sqlemp=mysqli_query($dbcon,"SELECT * FROM `tblnews` limit 3 ");
									while($rows=mysqli_fetch_array($sqlemp)){
								?> 
                                  
								  
                                    
								
										<div class="col-md-4">
                                         <img src="adminenrile/news/<?php echo $rows[5];?>" class="img img-responsive">
                                         <h3><?php echo $rows[1];?></h3>
                                     <p><strong>Author:</strong> <?php echo $rows[2];?></p> 
									<p><strong>Date:</strong> <?php echo $rows[3];?></p>
                                     <?php echo $rows[4];?><br>
                          
							 
							 </div>
                                     
									   <?php } ?>
       
<!--
        <div class="row-fluid bordertop">
          <p>   <h1>ANNOUNCEMENT</h1></p>
          <div class="span4">
            <ul class="services">
              <p>
 <div class="span4 side_bar">
      <section class="blog_cat">
        <h5></h5>
        <ul class="nav nav-pills">
          <li class="active"><a href="announcement.php">READMORE</a></li>
              
              </p>
              </ul>
      </section>
            </ul>
          </div>
          <div class="span4">
            <ul class="services">
            
            </ul>
          </div>
        </div>
-->
      </div>
<!--
      <div class="span4">
        <h2> <span class="">RECENT</span>NEWS</h2>
        <div class=""> <span class=""></span><span class="">
          <div class=""></div>
           <?php
									$sqlemp=mysqli_query($dbcon,"SELECT * FROM `tblnews` limit 3 ");
									while($rows=mysqli_fetch_array($sqlemp)){
								?>
                                
                                    <tr>
                                       <h3><?php echo $rows[1];?></h3><br>
                                       <?php echo $rows[2];?><br>
                                       sdfskfhdsgd
                                       <img src="adminenrile/news/<?php echo $rows[5];?>" ><br>

                                        <?php } ?> 
          </span> </div>
        <div class=""> <span class="icon coffee-day"></span><span class="benefit_text">
          <div class=""></div>
          <p></p>
          </span> </div>
        <div class=""> <span class="icon personal-projects"></span><span class="benefit_text">
          <div class=""></div>
          <p>
        </p>
        
 </div>
      </div>
-->
    </div>
  </div>
</div>
</div>


<?php
	include('footer.php');
?>