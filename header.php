<?php
	include('config.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>OFFICIALS WEBSITE OF ENRILE</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="assets/css/bootstrap.css" rel="stylesheet">
<link href="assets/css/bootstrap-responsive.css" rel="stylesheet">
<link href="assets/css/docs.css" rel="stylesheet">
<link href="assets/js/google-code-prettify/prettify.css" rel="stylesheet">
<!--[if lt IE 9]>
<script src="assets/js/html5.js"></script>
<![endif]-->
<link rel="shortcut icon" href="assets/ico/favicon.ico">
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">
</head>
<body data-spy="scroll" data-target=".bs-docs-sidebar">
<div class="">
  <div class="nav-agency">
    <div class="navbar navbar-static-top">
      <div class="navbar-inner">
        <div class="container"> <a class="brand" href="."> <img src="imgs/logo2.png" alt=""></a>
          <div id="main-nav">
            <div class="nav-collapse collapse">
              <ul class="nav">
                <li class="active"><a href="index.php">HOME</a> </li>
                <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">ABOUT US <b class="caret"></b></a>
                  <ul class="dropdown-menu">
					<li><a href="history.php">History</a></li>
					<li><a href="mission-vision.php">Mission & Vision</a></li>
                      <li><a href="barangay.php">Barangay</a></li>
                  </ul>
                </li>
                <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#"> GALLERY<b class="caret"></b></a>
                  <ul class="dropdown-menu">
                    <li><a href="festival.php">Mappalabbet Festival</a></li>
                    <li><a href="tourist-spot.php">Tourist Spot</a></li>
                  </ul>
                </li>
                <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"> OFFICIALS <b class="caret"></b></a>
                  <ul class="dropdown-menu">
                    <li><a href="elected-officials.php">Elected Official</a></li>
                    <li><a href="department-heads.php">Department Heads</a></li>
                  </ul>
                </li>
                   <li><a href="news-events.php">NEWS & EVENTS </a></li>
                <li><a href="contact.php">CONTACT</a> </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>